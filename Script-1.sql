CREATE TABLE Products (
ProductID INT PRIMARY KEY,
Productname varchar(40) NOT NULL,
SupplierID Int references Suppliers(SupplierID),
CategoryID int references Categories(CategoryID),
Unitprice int,
Unistock smallint,
Unitorder smallint,
readerleavel smallint,
discontinued bit
)
create table OrderDetails (
OrderID int REFERENCES Orders(OrderID),
ProductID int REFERENCES Products(ProductID),
Unitprice int,
Quavility smallint
)
create table Categories(
CategoryID int primary key,
CategoryName varchar(20),
description tinytext,
Picture text
)
create table Suppliers (
SupplierID int PRIMARY key,
CompanyName varchar(40),
ContactName varchar(30),
ContactTitle varchar(30),
Address varchar(60),
City varchar(15),
Region varchar(15),
PostalCode varchar(15),
Country varchar(15),
Phone varchar(15),
Fax varchar(24),
Homepage text
)
create table Shippers (
ShipperID int,
CustomerName varchar(40),
Phone varchar(24)
)
create table territories(
TerritoryID varchar(20) primary key,
TerritoryDescription varchar(50),
RegionID int references Region(RegionID)
)
create table Orders(
OrderID int primary key,
CustomerID int references Customers(CustomerID),
EmployeeID int references Employees(EmployeeID),
Orderdate datetime,
Shipname varchar(40),
freight text,
shipadress varchar(40),
Shipcity varchar(40),
Shipregion varchar(15),
Shippostalcode varchar(30),
Shipcountry varchar(15)
)
create table CustormerDemoGraphics(
CustomerTypeID char(10) primary key,
CustomerDecs text
)
create table CustomerCustorDemo(
CustomerID char(5) references Customers(CustomerID),
CustomerTypeID char(10) REFERENCES CustormerDemoGraphic(CustomerTypeID)
)
create table Employees(
EmployeesID int PRIMARY KEY,
LastName varchar(20),
FirstNam varchar(15),
Title varchar(30),
TitleCountry varchar(25),
Address varchar(64),
City varchar(20),
Region varchar(15),
PostalCode varchar(15),
Country varchar(15),
HomePhone varchar(24),
Extension varchar(4),
Photo text,
ReportTo int,
PhotoPath varchar(255)
)
create table EmployeeTerritories(
EmployeesID int REFERENCES Employees(EmployeesID),
TerritoryID varchar(20) REFERENCES Territories(TerritoryID)
)
create table Customers(
   CustomerID char(5) primary key,
   CompanyName varchar(50),
   ContactName varchar(50),
   ComtactTitle varchar(50),
   Address varchar(50),
   City varchar(50),
   Region varchar(50),
   AutoCode varchar(50),
   Country varchar(50),
   Phone varchar(50),
   Fax varchar(50)
)
create table Region(
   RegionID int primary key,
   RegionConscription char(50)
)
-- insert into products values (000001,N'S?n ph?m 1',00001,0001,120000)

create view viewtest as select * from customers

create view viewtest2 as select * from employees

select * from viewtest